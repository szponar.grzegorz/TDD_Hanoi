package Control;

import Model.Pegs;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class GameTests {

    @Test
    public void makeMove_CorrectlyRemovesAndSetsDiscs() throws Exception {
        Game testGame = new Game(3, 3);
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(0, 1, 2);
        testPegs.setDisc(2, 0, 1);

        testGame.getPegs().fillFirstPegWithDiscs(3);
        testGame.makeSingleMove();
        Pegs gamePegs = testGame.getPegs();

        assertTrue(testPegs.equals(gamePegs));
    }

    @Test
    public void startGame_PlaysUntilGameIsFinished() throws Exception {
        Game testGame = new Game(3, 3);
        testGame.start();

        assertTrue(Logic.isFinished(testGame.getPegs()));
    }
}
