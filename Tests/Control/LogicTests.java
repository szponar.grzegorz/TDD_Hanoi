package Control;

import Model.Pegs;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LogicTests {

    @Test
    public void TestThatVerifyOrder_IsCorrectlyCheckingDiscsOrder_Scenario1() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(0, 1, 2);
        testPegs.setDisc(0, 2, 1);
        testPegs.setDisc(1, 0, 2);
        testPegs.setDisc(1, 1, 1);
        testPegs.setDisc(2, 0, 3);
        testPegs.setDisc(2, 1, 1);

        assertTrue(Logic.verifyOrder(testPegs));
    }

    @Test
    public void VerifyDecideMove_DetectsSmallestDisc_MovesItFarRight() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(0, 1, 2);
        testPegs.setDisc(0, 2, 1);

        testPegs.setLastMove(0, 0, 3);

        int[] testRemovePosition = {0, 2, 1};
        int[] testSetPosition = {2, 0, 1};
        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyDecideMove_DetectsSmallestDisc_MovesItCloseLeft() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(1, 0, 2);
        testPegs.setDisc(2, 0, 1);
        testPegs.setLastMove(0, 0, 3);

        int[] testRemovePosition = {2, 0, 1};
        int[] testSetPosition = {1, 1, 1};
        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyDecideMove_BiggestDiscMoved_WhenSmallerCannotBeMoved() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(1, 0, 2);
        testPegs.setDisc(1, 1, 1);

        testPegs.setLastMove(1, 1, 1);

        int[] testRemovePosition = {0, 0, 3};
        int[] testSetPosition = {2, 0, 3};

        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyDecideMove_DiscThatCanBeMoveRightAndWereNotLastMove_isMoved() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 1);
        testPegs.setDisc(1, 0, 2);
        testPegs.setDisc(2, 0, 3);

        testPegs.setLastMove(0, 0, 1);

        int[] testRemovePosition = {1, 0, 2};
        int[] testSetPosition = {2, 1, 2};

        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyDecideMove_OnlyMovesThatFollowRulesAreAllowed() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(0, 1, 2);
        testPegs.setDisc(2, 0, 1);

        testPegs.setLastMove(2, 0, 1);

        int[] testRemovePosition = {0, 1, 2};
        int[] testSetPosition = {1, 0, 2};

        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyDecideMove_LastMoveIsForbidden_ThenMoveLeft() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(2, 0, 3);
        testPegs.setDisc(1, 0, 2);
        testPegs.setDisc(2, 1, 1);

        testPegs.setLastMove(2, 1, 1);

        int[] testRemovePosition = {1, 0, 2};
        int[] testSetPosition = {0, 0, 2};

        int[][] testPosition = Logic.decideMove(testPegs);

        assertArrayEquals(testRemovePosition, testPosition[0]);
        assertArrayEquals(testSetPosition, testPosition[1]);
    }

    @Test
    public void VerifyGameIsFinishedIsTrue_AllDiscAreOnFarRight_WithProperOrder() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(2, 0, 3);
        testPegs.setDisc(2, 1, 2);
        testPegs.setDisc(2, 2, 1);

        assertTrue(Logic.isFinished(testPegs));
    }

    @Test
    public void VerifyGameIsFinishedIsFalse_WhenNotAllDiscAreOnFarRight() throws Exception {
        Pegs testPegs = new Pegs(3, 3);

        testPegs.setDisc(0, 0, 3);
        testPegs.setDisc(2, 0, 2);
        testPegs.setDisc(2, 1, 1);

        assertFalse(Logic.isFinished(testPegs));
    }
}
