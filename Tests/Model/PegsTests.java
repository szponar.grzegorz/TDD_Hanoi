package Model;

import org.junit.Test;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PegsTests {

    @Test
    public void CreatingPegsObject_IsCreatingCorrectNumberOfPegs() throws Exception {
        int numberOfPegs = 5;
        int numberOfDiscs = 8;

        Pegs pegs = new Pegs(numberOfPegs, numberOfDiscs);

        assertEquals(numberOfPegs, pegs.getPegsLists().size());
    }

    @Test
    public void FillPegsWithDiscs_FillsFirstPeg_WithCorrectNumberOfDiscs() throws Exception {
        int numberOfPegs = 5;
        int numberOfDiscs = 8;

        Pegs pegs = new Pegs(numberOfPegs, numberOfDiscs);
        pegs.fillFirstPegWithDiscs(numberOfDiscs);

        assertEquals(numberOfDiscs, pegs.getPeg(0).size());
    }

    @Test (expected=IndexOutOfBoundsException.class)
    public void removeDisc_WillThrowIndexOutOfBoundsException_WhenTryToRemoveNotExistingDisc() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);

        actualPegs.fillFirstPegWithDiscs(3);
        actualPegs.removeDisc(0, 5);
    }

    @Test
    public void removeDisc_WillRemoveDiscCorrectly() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);
        Pegs expectedPegs = new Pegs(3, 3);

        expectedPegs.setDisc(0,0,3);
        expectedPegs.setDisc(0,1,2);

        actualPegs.fillFirstPegWithDiscs(3);
        actualPegs.removeDisc(0, 2);

        assertTrue(expectedPegs.equals(actualPegs));
    }

    @Test
    public void setDiscOnNotExistingPeg_WillNotThrowAnyExceptions() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);

        actualPegs.setDisc(5, 0, 1);
    }

    @Test
     public void setDisc_WillSetDiscCorrectly_scenario1() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);

        actualPegs.setDisc(2,0,1);

        assertTrue(actualPegs.getPeg(2).size() == 1);
    }

    @Test
     public void setDisc_WillSetDiscCorrectly_scenario2() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);

        actualPegs.setDisc(2,0,2);
        actualPegs.setDisc(2,1,1);

        assertTrue(actualPegs.getPeg(2).size() == 2);
    }

    @Test
    public void setLastMove_WillSetLastMoveCorrectly() throws Exception {
        Pegs actualPegs = new Pegs(3, 3);
        int valueOne = 0;
        int valueTwo = 1;
        int valueThree = 2;

        actualPegs.setLastMove(valueOne, valueTwo, valueThree);

        int[] actualLastMove = actualPegs.getLastMove();

        assertEquals(valueOne, actualLastMove[0]);
        assertEquals(valueTwo, actualLastMove[1]);
        assertEquals(valueThree, actualLastMove[2]);
    }

    @Test
    public void findMovableDisc_ThatDoNotExist_WillReturnEmptyArray() throws Exception {
        Pegs actualPegs = new Pegs(3,3);

        int[] testDisc = actualPegs.findMovableDisc(5);

        assertTrue(testDisc.length == 0);
    }

    @Test
    public void findMovableDisc_IfDiscCannotBeMoved_WillReturnEmptyArray() throws Exception {
        Pegs actualPegs = new Pegs(3,3);
        actualPegs.fillFirstPegWithDiscs(3);

        int[] testDisc = actualPegs.findMovableDisc(3);

        assertTrue(testDisc.length == 0);
    }

    @Test
    public void findMovableDisc_ReturnsCorrectPositionAndDiscValue() throws Exception {
        Pegs actualPegs = new Pegs(3,3);
        actualPegs.fillFirstPegWithDiscs(3);

        int[] expectedPosition = new int[] {0,2,1};
        int[] actualPosition = actualPegs.findMovableDisc(1);

        assertTrue(Arrays.equals(expectedPosition, actualPosition));
    }

    @Test
    public void findDisc_ThatDoNotExist_WillReturnEmptyArray() throws Exception {
        Pegs actualPegs = new Pegs(3,3);

        int[] testDisc = actualPegs.findDisc(5);

        assertTrue(testDisc.length == 0);
    }

    @Test
    public void findDisc_ReturnsCorrectPositionAndDiscValue() throws Exception {
        Pegs actualPegs = new Pegs(3,3);
        actualPegs.fillFirstPegWithDiscs(3);

        int[] expectedPosition = new int[] {0,0,3};
        int[] actualPosition = actualPegs.findDisc(3);

        assertTrue(Arrays.equals(expectedPosition, actualPosition));
    }
}
