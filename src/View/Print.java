package View;

import Model.Pegs;
import java.util.LinkedList;


public class Print {
    public static void printPegs(Pegs pegs) {
        for (int pegIndex = 0; pegIndex < pegs.getNumberOfPegs(); pegIndex++) {
            LinkedList<Integer> tempPeg = pegs.getPeg(pegIndex);
            for (Integer aTempPeg : tempPeg) {
                System.out.print(aTempPeg + " ");
            }
            System.out.println();
        }
    }
}
