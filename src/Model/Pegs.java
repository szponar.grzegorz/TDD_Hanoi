package Model;

import Control.Logic;
import java.util.LinkedList;


public class Pegs {
    private LinkedList<LinkedList<Integer>> pegs;
    private int numberOfPegs;
    private int numberOfDiscs;
    private int[] lastMove = new int[] {0,0,0};

    public Pegs(int numberOfPegs, int numberOfDiscs) {
        this.numberOfPegs = numberOfPegs;
        this.numberOfDiscs = numberOfDiscs;
        pegs = new LinkedList<>();
        creatingPegsList(numberOfPegs);
    }

    public int getNumberOfPegs() {
        return numberOfPegs;
    }

    public int getNumberOfDiscs() {
        return numberOfDiscs;
    }

    public LinkedList<LinkedList<Integer>> getPegsLists() {
        LinkedList<LinkedList<Integer>> tempPegs = new LinkedList<>();
        for (LinkedList<Integer> index : pegs)
            tempPegs.add(index);
        return tempPegs;
}

    public Pegs getClonedPegs() {
        Pegs clonedPegs = new Pegs(numberOfPegs, numberOfDiscs);
        for (int index = numberOfDiscs; index > 0; index--) {
            int[] disc = findDisc(index);
            if (disc.length != 0)
                clonedPegs.setDisc(disc[0], disc[1], disc[2]);
        }
        clonedPegs.setLastMove(lastMove);
        return clonedPegs;
    }

    public int[] findDisc(int discVariable) {
        for (int pegIndex = 0; pegIndex < getNumberOfPegs(); pegIndex++) {
            LinkedList<Integer> currentPeg = getPeg(pegIndex);
            for (int positionIndex = 0; positionIndex < currentPeg.size(); positionIndex++) {
                if (currentPeg.get(positionIndex) == discVariable)
                    return new int[]{pegIndex, positionIndex, discVariable};
            }
        }
        return new int[] {};
    }

    public int[] findMovableDisc(int discVariable) {
        for (int pegIndex = 0; pegIndex < getNumberOfPegs(); pegIndex++) {
            LinkedList<Integer> currentPeg = getPeg(pegIndex);
            for (int positionIndex = 0; positionIndex < currentPeg.size(); positionIndex++) {
                if (meetsTheConditionsOfTheCorrectMove(discVariable, currentPeg, positionIndex))
                    return new int[]{pegIndex, positionIndex, discVariable};
            }
        }
        return new int[] {};
    }

    public LinkedList<Integer> getPeg(int pegNumber) {
        LinkedList<Integer> originalPeg = pegs.get(pegNumber);
        LinkedList<Integer> tempPeg = new LinkedList<>();
        for (Integer index : originalPeg)
            tempPeg.add(index);
        return tempPeg;
    }

    public int[] getLastMove() {
        return lastMove;
    }

    public void setLastMove(int pegNumber, int discNumber, int discValue) {
        lastMove[0] = pegNumber;
        lastMove[1] = discNumber;
        lastMove[2] = discValue;
    }

    public void setLastMove(int[] lastMove) {
        this.lastMove = lastMove;
    }

    public void fillFirstPegWithDiscs(int numberOfDiscs) {
        LinkedList<Integer> currentPeg = pegs.get(0);
        for (int currentDisc = numberOfDiscs; currentDisc > 0; currentDisc--) {
            currentPeg.add(currentDisc);
        }
    }

    public void setDisc(int pegNumber, int discNumber, int discValue) {
        LinkedList<Integer> currentPeg, tempPeg;
        if (pegNumber >= 0 && pegNumber < pegs.size()){
            currentPeg = pegs.get(pegNumber);
            tempPeg = new LinkedList<>();

            for (Integer aCurrentPeg : currentPeg)
                tempPeg.add(aCurrentPeg);

            if (discNumber < 0 || discNumber > currentPeg.size())
                currentPeg.add(-1);
            else
                tempPeg.add(discValue);
                if (!Logic.verifyOrder(tempPeg))
                    currentPeg.add(-1);
                else
                    currentPeg.add(discValue);}
    }

    public void removeDisc(int pegNumber, int discNumber){
        LinkedList<Integer> currentPeg = pegs.get(pegNumber);
        currentPeg.remove(discNumber);
    }

        private void creatingPegsList(int numberOfPegs) {
            for (int numbers = 0; numbers < numberOfPegs; numbers++) {
                pegs.add(0, new LinkedList<>());
            }
        }

        private boolean meetsTheConditionsOfTheCorrectMove(int discVariable, LinkedList<Integer> currentPeg, int positionIndex) {
            return currentPeg.get(positionIndex) == discVariable
                    && isNotEqualToLastMove(discVariable)
                    && hasSameValueAsLastDiscInPeg(discVariable, currentPeg);
        }

        private boolean isNotEqualToLastMove(int discVariable) {
            return discVariable != lastMove[2];
        }

        private boolean hasSameValueAsLastDiscInPeg(int discVariable, LinkedList<Integer> currentPeg) {
            return discVariable == currentPeg.getLast();
        }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Pegs pegs1 = (Pegs) o;

        return pegs.equals(pegs1.pegs);
    }

}

