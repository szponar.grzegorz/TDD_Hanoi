package Control;

import Model.Pegs;
import View.Print;

public class Game {
    private Pegs pegs;

    public Game(int numberOfPegs, int numberOfDiscs) {
        pegs = new Pegs(numberOfPegs, numberOfDiscs);
    }

    public Pegs getPegs() {
        return pegs;
    }

    public void start() {
        pegs.fillFirstPegWithDiscs(pegs.getNumberOfDiscs());
        makeMove(pegs);
    }

    public void makeSingleMove() {
        int[][] decidedMove = Logic.decideMove(pegs);
        pegs.removeDisc(decidedMove[0][0], decidedMove[0][1]);
        pegs.setDisc(decidedMove[1][0], decidedMove[1][1], decidedMove[1][2]);
    }

    public Pegs makeMove(Pegs pegs) {
        makeSingleMove();

        Print.printPegs(pegs);
        System.out.println("-------------------------------");

        if (Logic.isFinished(pegs))
            return pegs;

        return makeMove(pegs);
    }
}
