package Control;

import Model.Pegs;
import java.util.LinkedList;


public class Logic {

    public static boolean verifyOrder(Pegs pegs) {
        for (int pegIndex = 0; pegIndex < pegs.getNumberOfPegs(); pegIndex++) {
            LinkedList<Integer> currentPeg = pegs.getPeg(pegIndex);
            int tempDiscNumber = 1000;
            for (Integer index : currentPeg) {
                if (tempDiscNumber < index)
                    return false;
                tempDiscNumber = index;
            }
        }
        return true;
    }

    public static boolean verifyOrder(LinkedList<Integer> currentPeg) {
            int tempDiscNumber = 1000;
        for (Integer index : currentPeg) {
            if (tempDiscNumber < index)
                return false;
            tempDiscNumber = index;
        }
        return true;
    }

    public static int[][] decideMove(Pegs pegs) {
        int[] moveRight, moveLeft;
        int discVariable = 1;

        while (discVariable <= pegs.getNumberOfDiscs()) {
            moveRight = new int[0];
            moveLeft = new int[0];
            int[] currentDisc = pegs.findMovableDisc(discVariable);

            if (currentDisc.length != 0){
                moveRight = moveRight(currentDisc, pegs);
                moveLeft = moveLeft(currentDisc, pegs);
            }

            if (moveRight.length != 0) {
                pegs.setLastMove(moveRight);
                return new int[][]{currentDisc, moveRight};
            } else if (moveLeft.length != 0) {
                pegs.setLastMove(moveLeft);
                return new int[][]{currentDisc, moveLeft};
            }
            discVariable++;
        }
        return new int[][]{};
    }

    public static boolean isFinished(Pegs pegs) {
        return verifyOrder(pegs) && allDiscsAreOnTheLastPeg(pegs);
    }

        private static boolean allDiscsAreOnTheLastPeg(Pegs pegs) {
            int disc = pegs.getNumberOfDiscs();
            int conditionNumber = 0;

            while(disc > 0){
                int[] discPosition = pegs.findDisc(disc);

                if (discPosition[0] == pegs.getNumberOfPegs() - 1)
                    conditionNumber++;
                disc--;
            }
            return conditionNumber == pegs.getNumberOfDiscs();
        }

        private static int[] moveRight(int[] currentDisc, Pegs pegs) {
            int pegNumber = 1;
            while (true) {
                Pegs tempPegs = pegs.getClonedPegs();
                int pegIndex = tempPegs.getNumberOfPegs() - pegNumber;
                int discIndex = tempPegs.getPeg(tempPegs.getNumberOfPegs() - pegNumber).size();

                if (pegIndex == currentDisc[0])
                    return new int[0];

                tempPegs.setDisc(pegIndex, discIndex, currentDisc[2]);
                if (tempPegs.getPeg(pegIndex).getLast() != -1) {
                    tempPegs.removeDisc(currentDisc[0], currentDisc[1]);
                    return new int[]{pegIndex, discIndex, currentDisc[2]};
                }

                if (pegNumber < pegs.getNumberOfPegs())
                    pegNumber++;
                else
                    return new int[0];
            }
        }

        private static int[] moveLeft(int[] currentDisc, Pegs pegs) {
            int pegNumber = 1;
            while (true) {
                Pegs tempPegs = pegs.getClonedPegs();
                int pegIndex = currentDisc[0] - pegNumber;
                if (pegIndex >= 0 && pegIndex < tempPegs.getNumberOfPegs()) {

                    int discIndex = tempPegs.getPeg(pegIndex).size();

                    if (pegIndex == currentDisc[0])
                        return new int[0];

                    tempPegs.setDisc(pegIndex, discIndex, currentDisc[2]);
                    if (tempPegs.getPeg(pegIndex).getLast() != -1) {
                        tempPegs.removeDisc(currentDisc[0], currentDisc[1]);
                        return new int[]{pegIndex, discIndex, currentDisc[2]};
                    }
                }
                if (pegNumber < currentDisc[0])
                    pegNumber++;
                else
                    return new int[0];
            }
        }
}
