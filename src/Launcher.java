import Control.Game;


public class Launcher {

    public static void main(String[] args) {
        Game game = new Game(3, 5);
        game.start();
    }
}
